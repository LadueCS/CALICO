import java.util.*;
import java.io.*;

public class Main {
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
	static StringTokenizer st;

	static String next() throws IOException {
		while (st == null || !st.hasMoreTokens())
			st = new StringTokenizer(br.readLine().trim());
		return st.nextToken();
	}

	static long readLong() throws IOException {
		return Long.parseLong(next());
	}

	static int readInt() throws IOException {
		return Integer.parseInt(next());
	}

	static double readDouble() throws IOException {
		return Double.parseDouble(next());
	}

	static char readCharacter() throws IOException {
		return next().charAt(0);
	}

	static String readLine() throws IOException {
		return br.readLine().trim();
	}

	public static void main(String[] args) throws IOException {
      int n = readInt ();

		for (int t = 0; t < n; t ++){
			int num = readInt ();
			//int [] arr = readLine().split(" ");
			//int [] votes = new int [num+2];
			
			Pair arr [] = new Pair [num + 2];

			for (int i = 0; i < num + 2; i ++){
				if (i == num || i == num + 1){
					arr[i] = new Pair (0, 1, i+1);
					continue;
				}
				arr[i] = new Pair (0, 0, i+1);
			}

			for (int i = 0; i < num; i ++){
				int target = readInt();
				arr[target-1] = new Pair (arr[target-1].x + 1, arr[target-1].y, arr[target-1].z);
			}

		   Compare obj = new Compare();
 
        	obj.compare(arr, num + 2);

			if (arr[num+1].y == 1){
				if (arr [num].y == 1){
					if (arr[num-1].x == arr[num].x && arr[num].x == arr[num+1].x){
						for (int i = num - 1; i >= 0; i --){
							if (arr[i].x != arr[num].x){
								System.out.println (arr[i+1].z +  " SKIP");
								break;
							}
						}
						continue;
					}
					else{
						if (arr [num-1].x + 2 < arr[num+1].x){
							System.out.println ("SKIP SKIP");
							continue;
						}
						else {
							for (int i = num-1; i >= 0; i --){
								if (arr[i].x != arr[num-1].x){
									System.out.println (arr[i+1].z +  " " + arr[i+1].z);
									break;
								}
							}
							continue;
						}
					}
				}
				else if (arr[num+1].x != arr[num].x){
					if (arr [num].x + 2 < arr[num+1].x){
						System.out.println ("SKIP SKIP");
						continue;
					}
					else {
						for (int i = num; i >= 0; i --){
							if (arr[i].x != arr[num].x){
								System.out.println (arr[i+1].z +  " " + arr[i+1].z);
								break;
							}
						}
						continue;
					}
				}
				else {
					for (int i = num; i >= 0; i --){
						if (arr[i].x != arr[num].x){
							System.out.println (arr[i+1].z + " SKIP");
							break;
						}
					}
				}
			}
			else {
				System.out.println (arr[0].z + " " + arr[0].z);
			}
		}
   }
}

class Compare {
 
	void compare(Pair arr[], int n)
	{
		 // Comparator to sort the pair according to second element
		 Arrays.sort(arr, new Comparator<Pair>() {
			  @Override public int compare(Pair p1, Pair p2)
			  {
					return p1.x - p2.x; // To compare the first element just
												 //change the variable from p1.y - p2.y to x.
			  }
		 });
	}
}

class Pair {
	int x;
	int y;
	int z;

	public Pair (){
		x = 0;
		y = 0;
		z = 0;
	}
	// Constructor
public Pair(int x, int y, int z)
	{
		 this.x = x;
		 this.y = y;
		 this.z = z;
	}
}
