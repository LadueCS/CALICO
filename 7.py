T = int(input())
for t in range(T):
    S = input()
    if S == "":
        S = input()
    N = int(input())
    codes = input().split()
    r = 0
    dic = list(range(26))
    for code in codes[::-1]:
        if code == 'R':
            r += 1
        elif code == 'A':
            for i in range(26):
                dic[i] = 25-dic[i]
        else:
            c = int(code[1:])
            for i in range(26):
                dic[i] -= c
                dic[i] %= 26
    r %= 2

    for i in S[::r*-2+1]:
        if(i.isalpha()):
            print(chr(dic[ord(i)-97]+97), end='')
        else:
            print(i, end='')
    print()
