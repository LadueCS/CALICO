#include <bits/stdc++.h>
#define f first
#define s second
using namespace std;
using ll = long long;
using ii = pair<int, int>;
const int MX = 1e5+5;

int main() {
	cin.tie(0)->sync_with_stdio(0);
	int T; cin >> T;
	while (T--) {
		int N; cin >> N;
		set<int> S;
		int ans = 0;
		for (int i = 0; i < N; ++i) {
			char A; int K; cin >> A >> K;
			if (A =='P'){
				S.insert(K);
			}
			else {
				S.erase(K);
			}
			if (S.size()) ans = max(*rbegin(S)-*begin(S), ans);
		}
		cout << ans << '\n';
	}	
	
}
