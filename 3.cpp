#include <bits/stdc++.h>
#define f first
#define s second
using namespace std;
using ll = long long;
using ii = pair<int, int>;
const int MX = 1e5+5;

int main() {
	cin.tie(0)->sync_with_stdio(0);
	int a[205];
	a[99] = -1, a[100] = 0, a[101] = 1;
	for (int i = 99; i < 200; ++i) a[i+3] = a[i+2]+a[i+1]+a[i];
	for (int i = 101; i > 10; --i) a[i-3] = a[i]-a[i-1]-a[i-2];
	int T; cin >> T;
	while (T--) {
		int N; cin >> N;
		cout << a[N+100] << '\n';
	}
	
}
