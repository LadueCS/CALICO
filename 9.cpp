#include <bits/stdc++.h>
#define f first
#define s second
using namespace std;
using ll = long long;
using ii = pair<int, int>;
const int MX = 1e5+5;

int dx[4] = {1,0,-1,0};
int dy[4] = {0,1,0,-1};

char G[305][305];
int dp[30][305][305];
ii pre[30][305][305];
char ans[305][305];

int main() {
	cin.tie(0)->sync_with_stdio(0);
	int T; cin >> T;
	while (T--) {
		string S; cin >> S;
		int R, C; cin >> R >> C;
		for (int i = 0; i <R; ++i) {
			for (int j = 0;j<C;++j)cin >> G[i][j];
		}
		for (int x = 0; x < S.size(); ++x) {
		for (int i = 0; i < R; ++i) {
			for (int j = 0; j < C; ++j) dp[x][i][j] = 0, pre[x][i][j] = ii(-1, -1);
		}
		}
		for (int i = 0; i < R; ++i) {
			for (int j = 0; j < C;++j) {
				if (G[i][j] == S[0]) dp[0][i][j] = 1;
			}
		}
		for (int x = 1; x < S.size(); ++x) {
			for (int i = 0; i < R; ++i) {
				for (int j = 0; j < C; ++j) if (S[x] == G[i][j]) {
					for (int d = 0; d < 4; ++d){
						int a = i+dx[d], b =j+dy[d];
						if (a >= 0 && a < R && b >= 0 && b < C && dp[x-1][a][b] == 1) {
							dp[x][i][j] = 1, pre[x][i][j] = ii(a, b);
							//cout << x << ' ' << i << ' ' << j << '\n';
						}
					}
				}
			}
		}
		for (int i = 0; i<R; ++i) {
			for (int j = 0;j<C; ++j) ans[i][j] = '#';
		}
		for (int i = 0; i < R; ++i) {
			for (int j = 0; j < C; ++j) if (dp[S.size()-1][i][j]) {
				ii cur(i, j);
				for (int x = S.size()-1; x >= 0; --x){
					ans[cur.f][cur.s] = S[x];
					cur = pre[x][cur.f][cur.s];
					}
			}
		}
		for (int i = 0; i<R;++i) {
			for (int j = 0;j<C;++j) cout << ans[i][j];
			cout << '\n';
		}

		
	}
	
}
