# run-error on second test case
from math import ceil
T = int(input())
for t in range(T):
    city = input()
    if city == "":
        city = input()
    city, N = city.split()
    N = int(N)
    taxis = []
    for i in range(N):
        taxi, b, m = input().split()
        b = int(b)
        m = int(m)
        taxis.append([m, b, taxi])
    taxis.sort()

    cur = 0
    for i in range(N):
        if taxis[i][1] < taxis[cur][1] or (taxis[i][1] == taxis[cur] and taxis[cur][0] > taxis[i][0]):
            cur = i

    print(city+':')
    # print(taxis)
    pos = 0
    while cur > 0:
        x, nc = 9999999, -1
        # print(666, taxis[cur])
        for i in range(0, cur):
            nx = ceil((taxis[i][1]-taxis[cur][1])/(taxis[cur][0]-taxis[i][0]))
            # print(nx, i, cur, 999)
            if nx < x or nx == x and taxis[i][0] < taxis[cur[0]]:
                nc = i
                x = nx
        if pos == nx-1:
            print(str(pos)+':', taxis[cur][2])
        else:
            print(str(pos)+'-'+str(nx-1)+':', taxis[cur][2])
        pos = nx
        cur = nc
    print(str(pos)+'+:', taxis[0][2])
    print()

'''
4
rio_de_janeiro 3
yellow_cab 12 237
blue_transit 1626 84
smart_car 799 100

palo_alto 1
uber 510 137

berkeley 4
red_bus 0 1611
green_bus 0 1610
blue_bus 123 456
yellow_bus 2034 455

hangzhou 3
fly_taxicab 1134 211
premium_cab 753 211
blue_line 2649 0
'''
