#include <bits/stdc++.h>
#define f first
#define s second
using namespace std;
using ll = long long;
using ii = pair<int, int>;
const int MX = 1e5+5;

int main() {
	cin.tie(0)->sync_with_stdio(0);
	
	int T;cin>>T;
	while(T--){
		string S;
		int N;cin>>S>>N;
		vector<pair<ii, string>> v;
		for (int i = 0; i <N;++i) {
			string s;
			int B, M;cin>>s>>B>>M;
			v.emplace_back(ii(M,B), s);
		}
		sort(begin(v), end(v));
		vector<pair<ii, int>> l;
		for (int i = N-1;i>=0; --i){
			while (l.size()) {
				int j = l.back().s;
				if (v[i].f.f == v[j].f.f) {l.pop_back(); continue;}
				int x = (v[j].f.s-v[i].f.s + v[i].f.f-v[j].f.f+1)/(v[i].f.f-v[j].f.f);
				//cout << x << '\n';
				if (x <= l.back().f.f) l.pop_back();
				else break;
			}
			if (l.size()) {
				int j = l.back().s;
				int x = (v[j].f.s-v[i].f.s + v[i].f.f-v[j].f.f+1)/(v[i].f.f-v[j].f.f);
				l.back().f.s = x-1;
				l.emplace_back(ii(x,1e9), i);
			}
			else l.emplace_back(ii(0, 1e9), i);
		}
		cout << S << ":\n";
		for (int i = 0; i < l.size(); ++i) {
			if (l[i].f.f == l[i].f.s) cout << l[i].f.f;
			else if (l[i].f.s == 1e9) cout << l[i].f.f << '+';
			else cout << l[i].f.f << '-' << l[i].f.s;
			cout << ": " << v[l[i].s].s << '\n';
		}
		if (T) cout << '\n';
	}
}
