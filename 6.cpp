#include <bits/stdc++.h>
#define f first
#define s second
using namespace std;
using ll = long long;
using ii = pair<int, int>;
const int MX = 1e5+5;

int ans[1000][1000];

void sets(int s, int x, int y) {
	if (s == 1) {
		ans[x][y] = 1;
		ans[x+1][y] = 1;
		return;
	}
	sets(s/2, x+s/2, y);
	sets(s/2, x, y+s/2);
	sets(s/2, x+s, y+s/2);
}

int main() {
	cin.tie(0)->sync_with_stdio(0);
	int T; cin >> T;
	while (T--) {

	
		memset(ans, 0, sizeof ans);
		int N; cin >> N;
		if (N == 1) {
			cout << "/\\\n";
			continue;
		}
		sets(N, 0, 0);
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j <= N+i; ++j) {
				if (ans[j][i] == 0) cout << ' ';
				else if ((i+j) & 1) cout << '/';
				else cout << '\\';
			}
			cout << '\n';
		}
	}	
	
}
