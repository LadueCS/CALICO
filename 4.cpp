#include <bits/stdc++.h>
#define f first
#define s second
using namespace std;
using ll = long long;
using ii = pair<int, int>;
const int MX = 1e5+5;

char G[305][305];
char H[305][305];

int main() {
	cin.tie(0)->sync_with_stdio(0);
	
	int T;cin>>T;
	while(T--){
		string S;cin>>S;
		int R,C;cin>>R>>C;
		for (int i=0;i<R;++i) {
			for (int j = 0;j<C;++j){
				cin >>G[i][j];
			}
		}
		for (int i = 0; i <R;++i){
			for (int j = 0;j<C;++j) H[i][j] = '#';
		}
		int N =S.size();
		for (int i = 0; i < R;++i) {
			for (int j = 0; j <C;++j) {
				int ans = 1;
				for (int k = 0;k<N;++k) if (i+k >= R || G[i+k][j] != S[k]) ans = 0;
				if (ans) {
					for (int k = 0;k<N;++k) H[i+k][j] = S[k];
					goto DONE;
				}
				ans = 1;
				for (int k = 0; k<N;++k) if (j+k >=C||G[i][j+k] != S[k]) ans = 0;
				if (ans) for (int k = 0;k<N;++k) {
					H[i][j+k] = S[k];
					goto DONE;
				}
				ans = 1;
				for (int k=0;k<N;++k)if(i-k<0||G[i-k][j] !=S[k])ans=0;
				if (ans)for (int k=0;k<N;++k){
				H[i-k][j] = S[k];
				goto DONE;
				}
				ans=1;
				for(int k=0;k<N;++k)if(j-k<0||G[i][j-k]!=S[k])ans=0;
				if(ans){for(int k=0;k<N;++k)
				H[i][j-k]=S[k];
				goto DONE;
				}
				
				
			}
		}
		DONE:for (int i =0;i<R;++i){
			for (int j=0;j<C;++j)cout <<H[i][j];
			cout << '\n';
		}
	}
}
