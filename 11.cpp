#include <bits/stdc++.h>
#define f first
#define s second
using namespace std;
using ll = long long;
using ii = pair<int, int>;
const int MX = 1e5+5, MOD=1e9+7;

ll DP[2][4][1000];

int main() {
	cin.tie(0)->sync_with_stdio(0);
	int T;cin>>T;
	while(T--){
		memset(DP, 0, sizeof DP);
		int N; cin>>N;
		DP[0][0][1] = DP[1][0][1] = 1;
		for (int i = 1; i < N; ++i) {
			// (X)
			(DP[0][0][i+2] += DP[0][0][i]+DP[0][1][i]+DP[0][2][i]+DP[0][3][i]) %= MOD;
			(DP[1][0][i+2] += DP[1][0][i]+DP[1][1][i]+DP[1][2][i]+DP[1][3][i]) %= MOD;
			// !X
			(DP[0][1][i+1] += DP[1][0][i]+DP[1][1][i]) %= MOD;
			(DP[1][1][i+1] += DP[0][0][i]+DP[0][1][i]) %= MOD;

			// X&Y
			for (int a = 1; a <= i; ++a) {
			for (int j = 1; j <= i; ++j) {
			    if (a != i && j != i) continue;
				ll sum = (DP[1][0][a]+DP[1][1][a]+DP[1][2][a])%MOD*(DP[1][0][j]+DP[1][1][j])%MOD;
				(DP[1][2][a+j+1] += sum) %= MOD;
				(DP[0][2][a+j+1] += (DP[0][0][a]+DP[0][1][a]+DP[0][2][a]+DP[1][0][a]+DP[1][1][a]+DP[1][2][a])%MOD*(DP[0][0][j]+DP[0][1][j]+DP[1][0][j]+DP[1][1][j])%MOD-sum+MOD) %= MOD;
			}
			}
			// X|Y
			for (int a = 1; a<=i;++a){
			for (int j = 1; j<=i; ++j) {
				if (a != i && j != i) continue;
				ll sum = (DP[0][0][a]+DP[0][1][a]+DP[0][2][a]+DP[0][3][a])%MOD*(DP[0][0][j]+DP[0][1][j]+DP[0][2][j])%MOD;
				(DP[0][3][a+j+1] += sum) %= MOD;
				(DP[1][3][a+j+1] += (DP[0][0][a]+DP[0][1][a]+DP[0][2][a]+DP[0][3][a]+DP[1][0][a]+DP[1][1][a]+DP[1][2][a]+DP[1][3][a])%MOD*(DP[0][0][j]+DP[0][1][j]+DP[0][2][j]+DP[1][0][j]+DP[1][1][j]+DP[1][2][j])%MOD-sum+MOD) %= MOD;
			}
			}
		}
		/*for (int k = 1; k <= 5; ++k) {
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 4; ++j) {
				cout << DP[i][j][k] << ' ';						
			}
		}
		cout << '\n';
		}*/
		cout << (DP[1][0][N]+DP[1][1][N]+DP[1][2][N]+DP[1][3][N])%MOD << '\n';
	}
	
}
