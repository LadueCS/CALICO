import java.util.*;
import java.io.*;

public class Main {
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
	static StringTokenizer st;

	static String next() throws IOException {
		while (st == null || !st.hasMoreTokens())
			st = new StringTokenizer(br.readLine().trim());
		return st.nextToken();
	}

	static long readLong() throws IOException {
		return Long.parseLong(next());
	}

	static int readInt() throws IOException {
		return Integer.parseInt(next());
	}

	static double readDouble() throws IOException {
		return Double.parseDouble(next());
	}

	static char readCharacter() throws IOException {
		return next().charAt(0);
	}

	static String readLine() throws IOException {
		return br.readLine().trim();
	}

	public static void main(String[] args) throws IOException {
      int n = readInt();

      for (int i = 0; i < n; i ++){
         String str = readLine ();
         int col = Integer.parseInt(str.substring (0, str.indexOf ('x')));
         int row  = Integer.parseInt (str.substring (str.indexOf('x')+1));
         for (int j = 0; j < col; j ++){
            for (int k = 0; k < row; k ++){
               if (j %2 == 0){
                  if (k%2 == 0){
                     System.out.print ("B");
                  }
                  else {
                     System.out.print ("G");
                  }
               }
               else {
                  if (k%2 == 0){
                     System.out.print ("G");
                  }
                  else {
                     System.out.print ("R");
                  }
               }
            }
            System.out.println();
         }

      }
   }
}
